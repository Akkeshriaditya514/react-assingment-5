import React from "react";
import { connect } from "react-redux";
const CompanyDetail = (props) => {
  if (!props.selectedCompany) {
    return <h2> </h2>;
  }
  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          width: "100%",
        }}
      >
        <h3>{props.selectedCompany.name}</h3>
      </div>
      <div style={{ margin: "20px" }}>
        {props.selectedCompany.jobs.map((job, index) => {
          const { title, salary, jobType } = job;
          return (
            <div
              className="ui card"
              style={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div
                className="content"
                style={{
                  fontSize: "15px",
                  fontWeight: "bolder",
                  padding: "5px",
                  margin: "0px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {title}
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  fontSize: "12px",
                  fontWeight: "bold",
                  padding: "0px",
                  margin: "0px",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {job.locations.map((location) => {
                  return <div>{location},</div>;
                })}
              </div>
              <img
                src="https://picsum.photos/id/180/200/100"
                alt="logo"
                style={{ width: "100%" }}
              />
              <div style={{ padding: "10px" }}>
                <span style={{ fontWeight: "bold" }}>Job Type : </span>
                {jobType}
                <div style={{ paddingTop: "10px" }}>
                  <span
                    style={{
                      fontWeight: "bold",
                      display: "flex",
                      flexDirection: "column",
                    }}
                  >
                    Requirements: <br />
                  </span>
                  {job.requirements.map((requirement) => {
                    return (
                      <div style={{ fontSize: "13px" }}>{requirement},</div>
                    );
                  })}
                  <div style={{ paddingTop: "10px" }}>
                    <span style={{ fontWeight: "bold" }}> Salary : </span>
                    {salary}
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return { selectedCompany: state.selectompany };
};

export default connect(mapStateToProps)(CompanyDetail);
