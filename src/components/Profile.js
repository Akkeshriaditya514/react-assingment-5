import React, { Component } from "react";
import PersonalInformation from "./PersonalInformation";
import Skills from "./Skills";
import WorkExperienceForm from "./WorkExperienceForm";
import Education from "./Education";
import Header from "./Header";

export default class Profile extends Component {
  render() {
    return (
      <div style={{ padding: "50px" }}>
        <Header />

        <div className="profile">
          <PersonalInformation />

          <WorkExperienceForm />

          <Education />

          <Skills />
        </div>
      </div>
    );
  }
}
